provider "aws" {
    region     = "us-east-1"
    profile = "default"
 
 
}

variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "avail_zone" {}
variable "env_prefix" {}


variable "instancetype" {
  default = "t2.micro"
  
}

variable "Image" {
  default = "ami-064d05b4fe8515623"
  
}

resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    "Name" = "${var.env_prefix}-vpc"
      }
  
}

resource "aws_subnet" "myapp_subnet" {
  vpc_id = aws_vpc.myapp-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.avail_zone
  tags = {
    "Name" = "${var.env_prefix}-subnet1"
  }
  
}

resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.myapp-vpc.id

  route  {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.my-app-ig.id
  }

  
}

resource "aws_internet_gateway" "my-app-ig" {
     vpc_id = aws_vpc.myapp-vpc.id
     tags = {
       "name" = "${var.env_prefix}-igw"
     }
}

resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id = aws_subnet.myapp_subnet.id
  route_table_id = aws_route_table.route_table.id
  
}

resource "aws_security_group" "myapp-sg" {
  name = "myapp-sg1"
  vpc_id = aws_vpc.myapp-vpc.id

  ingress {
    from_port = 3389
    to_port = 3389
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }
  
   ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   }

    egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
    description = "Out-going-full-access"
    }

    tags = {
      Name: "${var.env_prefix}-sg"
    }


}
data "aws_ami" "latest-amazon-linux-image" {
  most_recent = ture
  owners = ["amazon"]
  filter {
    name = "name"
    value = ["Amazon Linux 2 Kernel 5"]

  }
  filter {
    name = "virtualization-type"
    value = ["hvm"]
  }
  
}

output "aws_ami_id" {
   
  
}

resource "aws_instance" "myapp-srv1" {
  ami = var.Image
  
}